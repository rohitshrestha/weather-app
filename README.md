Test Using Postman
API:

1: API to get the forecast details using location key
localhost:8090/forecast?locationKey=459111


2: API to get the location details including city ,country state
localhost:8090/forecast/cities?city=pittsburg

3: API to get the location key using city name
localhost:8090/forecast/locationKey?city=pittsburg
