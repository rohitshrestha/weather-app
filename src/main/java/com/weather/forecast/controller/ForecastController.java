package com.weather.forecast.controller;

import com.weather.forecast.data.Location;
import com.weather.forecast.data.Weather;
import com.weather.forecast.dto.ErrorMessageDto;
import com.weather.forecast.service.AccuWeatherApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Optional;

@RestController
@RequestMapping("/forecast")
public class ForecastController {

    @Autowired
    AccuWeatherApiService accuWeatherApiService;

    @GetMapping(value = "/locationKey")
    public ResponseEntity<?> getLocationKey(@RequestParam("city") String city) throws IOException {
        Optional<Location> locationOptional = accuWeatherApiService.getLocation(city);

        if (!locationOptional.isPresent()) {
            return new ResponseEntity<Object>(
                    new ErrorMessageDto(
                            "Can not find location of US " + city),
                    HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(locationOptional.get().getLocationKey(), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<?> getWeatherForecast(@RequestParam("locationKey") String locationKey) throws IOException {
        Optional<Weather> weatherOptional = accuWeatherApiService.getWeather(locationKey);

        if (!weatherOptional.isPresent()) {
            return new ResponseEntity<Object>(
                    new ErrorMessageDto(
                            "Can not find forecast of locationkey " + locationKey),
                    HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(weatherOptional.get(), HttpStatus.OK);
    }

    @GetMapping(value = "/cities")
    public ResponseEntity<?> getCity(@RequestParam("city") String city) throws IOException {
        Optional<Location> locationOptional = accuWeatherApiService.getCity(city);

        if (!locationOptional.isPresent()) {
            return new ResponseEntity<Object>(
                    new ErrorMessageDto(
                            "Can not find location of US " + city),
                    HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(locationOptional.get(), HttpStatus.OK);
    }

}
