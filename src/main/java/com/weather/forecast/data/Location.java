package com.weather.forecast.data;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;

import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Location {
    private final String locationKey;

    @JsonProperty("country")
    private String country;

    @JsonProperty("state")
    private String administrativeArea;

    @JsonProperty("city")
    private String englishName;

    @JsonCreator
    public Location(@JsonProperty("Key") String locationKey,
                    @JsonProperty("Country") Map<String, String> country,
                    @JsonProperty("AdministrativeArea") Map<String,String> administrativeArea,
                    @JsonProperty("EnglishName") String englishName) {
        this.locationKey = locationKey;
        this.country = country.get("EnglishName");
        this.administrativeArea = administrativeArea.get("EnglishName");
        this.englishName = englishName;
    }


    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAdministrativeArea() {
        return administrativeArea;
    }

    public void setAdministrativeArea(String administrativeArea) {
        this.administrativeArea = administrativeArea;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getLocationKey() {
        return locationKey;
    }

    @Override
    public String toString() {
        return "Location{" +
                "locationKey='" + locationKey + '\'' +
                ", country='" + country + '\'' +
                ", state='" + administrativeArea + '\'' +
                ", city='" + englishName + '\'' +
                '}';
    }
}
