import com.weather.forecast.data.Location
import com.weather.forecast.dto.ErrorMessageDto
import com.weather.forecast.service.AccuWeatherApiService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.test.web.servlet.MockMvc
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.client.RestTemplate
import spock.lang.Specification

class ForecastTest extends Specification {
    RestTemplate restTemplate= new RestTemplate()

    @Autowired
    private MockMvc mvc
    def "Test Weather Api Key" () {
        setup:
        def accuWeatherService = new AccuWeatherApiService('VXLMAMCUllfjc3VwPwu98pCtvMKKvd4o', 'US', restTemplate)
        when:
        def testResult = accuWeatherService.getWeather('77385')

        then:
        mvc.perform(testResult).contentType("application/json")
                .andExpect(status().isOk()).andReturn().response
    }

//        def "Test Weather Api Key" (){
//        Object country= ['test']
//        Object admin= ['test']
//            Location location = new Location("12345", country as Map<String, String>, admin as Map<String, String>,'test')
//
//            expect:
//            location.getLocationKey() == "12345"
//            location.getCountry("EnglishName")== "test"
//            location.getAdministrativeArea("EnglishName") == "test"
//            location.getEnglishName() == "test"
//
//    }

//    @Autowired
//    private MockMvc mvc
//
//    def "when get is performed then the response has status 200 and content is 'Hello world!'"() {
//        expect: "Status is 200 and the response is 'Hello world!'"
//        mvc.perform(get("/forecast"))
//                .andExpect(status().isOk())
//                .andReturn()
//                .response
//    }
//
//    def "test mock" (){
//        expect:
//        Optional<Location> locationOptional = accuWeatherApiService.getCity(city);
//
//    }
//
//    public ResponseEntity<?> getCity(@RequestParam("city") String city) throws IOException {
//        Optional<Location> locationOptional = accuWeatherApiService.getCity(city);
//
//        if (!locationOptional.isPresent()) {
//            return new ResponseEntity<Object>(
//                    new ErrorMessageDto(
//                            "Can not find location of US " + city),
//                    HttpStatus.NOT_FOUND);
//        }
//        return new ResponseEntity(locationOptional.get(), HttpStatus.OK);
//    }
//

}
